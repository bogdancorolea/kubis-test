module.exports = {
    css: {
      loaderOptions: {
        sass: {
          sassOptions: {
            additionalData: `
            @import "@/styles/base/_variables.scss";
            @import "@/styles/base/_fonts.scss";
            `
          }
        }
      }
    },
    chainWebpack: config => {
      config.plugin("html").tap(args => {
        args[0].title = "Kubis Test";
        return args;
      });
    }
  };
  